﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AM4.Business.Models
{
    public class Noticia : Entity
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Imagem { get; set; }
    }
}
