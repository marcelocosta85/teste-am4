﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AM4.Business.Models
{
    public abstract class Entity
    {
        protected Entity()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
    }
}
