﻿using AM4.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AM4.Business.Interfaces
{
    public interface INoticiaRepository : IRepository<Noticia>
    {
    }
}
