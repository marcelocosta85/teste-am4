﻿using AM4.Business.Notificacoes;
using System;
using System.Collections.Generic;
using System.Text;

namespace AM4.Business.Interfaces
{
    public interface INotificador
    {
        bool TemNotificacao();
        List<Notificacao> ObterNotificacoes();
        void Handle(Notificacao notificacao);
    }
}
