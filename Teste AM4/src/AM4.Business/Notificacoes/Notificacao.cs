﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AM4.Business.Notificacoes
{
    public class Notificacao
    {
        public Notificacao(string mensagem)
        {
            Mensagem = mensagem;
        }

        public string Mensagem { get; }
    }
}
