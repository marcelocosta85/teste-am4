﻿using AM4.Business.Interfaces;
using AM4.Business.Models;
using AM4.Data.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace AM4.Data.Repository
{
    public class NoticiaRepository : Repository<Noticia>, INoticiaRepository
    {
        public NoticiaRepository(AM4DbContext context) : base(context)
        {
        }
    }
}
