﻿using AM4.Business.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace AM4.Data.Context
{
    public class AM4DbContext : DbContext
    {
        public AM4DbContext(DbContextOptions options) : base(options) { }

        public DbSet<Noticia> Noticias { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AM4DbContext).Assembly);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            base.OnModelCreating(modelBuilder);
        }
    }
}
