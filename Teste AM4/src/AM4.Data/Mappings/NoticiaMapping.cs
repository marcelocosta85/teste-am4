﻿using AM4.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace AM4.Data.Mappings
{
    public class NoticiaMapping : IEntityTypeConfiguration<Noticia>
    {
        public void Configure(EntityTypeBuilder<Noticia> builder)
        {
            builder.HasKey(n => n.Id);

            builder.Property(n => n.Titulo)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(n => n.Descricao)
                .IsRequired()
                .HasColumnType("varchar(1000)");
            builder.Property(n => n.Imagem)
                .IsRequired()
                .HasColumnType("varchar(200)");

            builder.ToTable("Noticias");
        }
    }
}
